#Author: Michał Misztal (ciemna.strona.klamki@gmail.com)

def sortDict(sl, sort='desc'):
    t = {}
    values = list(sl.values())
    if sort == 'asc':
        try:
            values.sort(reverse=True)
        except TypeError:
            return sl
    else:
        try:
            values.sort()
        except TypeError:
            return sl
    while len(values) > 0:
        w = values.pop()
        for k, v in sl.items():
            if v == w:
                t[k]=v
                continue
    return t