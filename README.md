python_dictionary_sort

Well, I wrote this as excercise because Python don't have that ability.

Example usage:

p1 = {'a' : [6,6], 'b' : 15, 'c': 45, 't':1, 'y':4}
p2 = {'a':'a','m':'m','k':'k'}
p3 = {'a' : 6, 'b' : 15, 'c': 45, 't':1, 'y':4}

print(sortuj(p1,'asc'))
print(sortuj(p2))
print(sortuj(p3,'asc'))

Output:

{'a': [6, 6], 'b': 15, 'c': 45, 't': 1, 'y': 4}
{'m': 'm', 'k': 'k', 'a': 'a'}
{'t': 1, 'y': 4, 'a': 6, 'b': 15, 'c': 45}